####English version below####
<br/><br/>

Hier folgt die Sammlung sämtlicher Informationen zum Thema Linux Router Hardware.
<br/><br/>

# Erwartungen an die Hardware
- AES-NI (intel) Unterstützung für ordentenliche VPN-Kompatibilität.
- Mehrere Access-Points sollen eingerichtet werden können
- mindestens 4 LAN Ports
- 2,4 GHz W-LAN
- 5 GHz W-LAN
- Möglichst Energie effizient
- Im besten Falle open Hardware
<br/><br/>

# Hardware Anforderungen
Die folgenden Anforderungen stammen aus der OPNsense Dokumentation. 
<br/><br/>

## Allgemeine Anforderungen
Für wesentlich eingeschränkte OPNsense® Funktionalität gibt es die Basisspezifikation. Für die volle Funktionalität gibt es minimale, angemessene und empfohlene Spezifikationen.

### Spezifikationen
| Hardware              | Minimum                                                                            | Angemessen                                                                         | Empfohlen                         |
|-----------------------|------------------------------------------------------------------------------------|------------------------------------------------------------------------------------|-----------------------------------|
| Prozessor             | 1 GHz dual core cpu                                                                | 1 GHz dual core cpu                                                                | 1.5 GHz multi core cpu            |
| RAM                   | 2 GB                                                                               | 4 GB                                                                               | 8 GB                              |
| Installations Methode | Serielle Konsole oder Video (VGA)                                                  | Serielle Konsole oder Video (VGA)                                                  | Serielle Konsole oder Video (VGA) |
| Installations Ziel    | SD- oder CF-Karte mit mindestens 4 GB, verwenden Sie Nano-Images zur Installation. | SD- oder CF-Karte mit mindestens 4 GB, verwenden Sie Nano-Images zur Installation. | 120 GB SSD                        |


<br/><br/>

### Was bedeutet das nun?
#### Minimum
Die minimale Spezifikation, um alle OPNsense Standardfunktionen auszuführen, die keine Schreibzugriffe auf die Festplatte benötigen, bedeutet, dass Sie alle Standardfunktionen ausführen können, mit Ausnahme derer, die Schreibzugriffe auf die Festplatte benötigen, z. B. ein Caching-Proxy (Cache) oder Intrusion Detection and Prevention (Alarmdatenbank).

#### Angemessen
Die angemessene Spezifikation, um alle OPNsense Standardfunktionen auszuführen, bedeutet, dass jede Funktion funktioniert, aber vielleicht nicht bei vielen Benutzern oder hoher Last.

#### Empfohlen
Die empfohlene Spezifikation, um alle OPNsense Standardfunktionen auszuführen, bedeutet, dass jede Funktion funktional ist und für die meisten Anwendungsfälle geeignet ist.

### Durchsatz
Die wichtigsten beteiligten Hardware-Faktoren des OPNsense-Setups sind CPU, RAM, Massenspeicher (Diskette), die Anzahl und Qualität der Netzwerkschnittstellen.

| Durchsatz (Mbps) | Hardware Anforderungen | Funktionsumfang | Benutzer / Netzwerke       |
|-------------------|-----------------------|-------------|------------------------|
| 11-150            | Basic Spez.           | minimal    | eingestellt (10-30)       |
| 11-150            | Minimum Spez.         | reduziert     | eingestellt (10-30)       |
| 151-350           | Angemessene Spez.      | vollständig         | wesentlich (30-50)    |
| 350-750+          | Empfohlene Spez.     | vollständig         | wesentlich+ (50-150+) |

<br/><br/>

### Weitere Informationen
Während die meisten Merkmale keinen Einfluss auf die Hardware-Dimensionierung haben, wirken sich einige wenige Merkmale massiv auf diese aus. Die Kandidaten sind:

**Squid** <br/>
ein Caching-Web-Proxy, der zur Kontrolle von Web-Inhalten verwendet werden kann. Diese Pakete sind stark auf CPU-Last und Festplatten-Cache-Schreibvorgänge angewiesen.

**Captive portal** <br/>
Einstellungen mit Hunderten von gleichzeitig bedienten Captive-Portal-Benutzern benötigen bei allen unten angezeigten Hardware-Spezifikationen mehr CPU-Leistung.

**State transition tables** <br/>
es ist bekannt, dass jeder Statustabelleneintrag etwa 1 kB (Kilobyte) RAM benötigt. Die durchschnittliche Zustandstabelle, gefüllt mit 1000 Einträgen, belegt etwa ~10 MB (Megabyte) RAM. OPNsense-Nutzungseinstellungen mit hunderttausenden von Verbindungen werden entsprechend Speicher benötigen.

<br/><br/>

# Geräte
## All-in-One Geräte
- Turris Omnia: https://www.turris.com/en/omnia/overview/
- OPNsense: https://shop.opnsense.com/
- Traverse Ten64: https://www.crowdsupply.com/traverse-technologies/ten64
- Protectli: https://protectli.com/vault-6-port/
- LES compact 4L: https://www.thomas-krenn.com/de/produkte/low-energy-systeme/les-compact-4l/les-compact-4-l.html
- NRG Systems: https://www.ipu-system.de/
- AWOW AK34 Mini PC: https://www.yuenx.com/2019/review-awow-ak34-mini-pc/
<br/><br/>

## Entwickler-Boards
- jetway nf592-q170: https://www.jetwaycomputer.com/NF592.html
- Apu Board: https://www.pcengines.ch/apu2.htm
- COMMELL: http://www.commell.com.tw/
- BananaPi BPI R64: http://www.banana-pi.com/eacp_view.asp?id=129 
<br/><br/>

### Hardware Konfiguration
#### Bauvorschlag
| Bauvorschlag  | Vorgeschlagene Hardware                                        | Preis in Euro |
|---------------|----------------------------------------------------------------|---------------|
| Prozessor     | INTEL Celeron G5900T 3.2GHz LGA1200 2M Cache Tray              | 45            |
| RAM           | 8GB G.Skill Aegis DDR4-2400 DIMM CL17 Single                   | 50            |
| Mainboard     | Gigabyte B460M Gaming HD, µATX, So. 1200                       | 75            |
| CPU-Lüfter    | Xilence I250PWM Topblow Kühler                                 | 10            |
| Netzteil      | 300 Watt be quiet! System Power B9 Bulk Non-Modular 80+ Bronze | 60            |
| Gehäuse       | Kolink KLM-002 Micro-ATX Mini-Gehäuse - black                  | 35            |
| Netzwerkkarte | -                                                              | 50-100        |
| Speicher      | 120GB HP EX900 M.2 2280 PCIe 3.0 x4 32Gb/s 3D-NAND TLC         | 25            |
|               |                                                                |               |
| **Gesamt**    |                                                                | **350 - 400** |

<br/>

**TDP: geschätzt 50 Watt**

<br/><br/>

#### Entwicklerboard Vorschlag
| Entwicklerboard Vorschlag | Vorgeschlagene Hardware                                      | Preis in Euro |
|---------------------------|--------------------------------------------------------------|---------------|
| Entwicklerboard           | PC Engines APU.4D4 Board (4 GByte, 4xLAN)                    | 155           |
| Gehäuse                   | PC Engines in schwarz, blau oder rot                         | 13            |
| Netzteil                  | PC Engines                                                   | 6             |
| Speicher                  | Transcend MTS430S M.2 SATA SSD 256GB inkl. Adapter           | 48            |
|                           |                                                              |               |
| Verfügbare Optionen       |                                                              |               |
| Netzwerkkarte             | Compex WLE200NX 802.11a/b/g/n (Wi-Fi 4) Mini-PCIe-Karte      | 23            |
| Antennenkabel             | 2x Pigtail von U.FL auf RP-SMA 15cm                          | 8             |
| Antenne                   | 2x 5dBi Wireless LAN Dual-Band Antenne mit RP-SMA-Anschluss  | 8             |
| Vorinstallation OPNsense | OPNsense                                                    | 10            |                          |                           |                                                              |               |
| **Gesamt**                |                                                              | **222** bzw. **271**   |

<br/>

**TDP: 6-12 Watt**

<br/><br/>

# Vor- und Nachteile
Hier werden die jeweiligen Vor- und Nachteile zwischen dem Bauvorschlag, den Entwicklerboards und den All-in-One Geräten und aufgeführt.

## Vorteile
| Bauvorschlag                                  | Vorschlag Entwicklerboard (APU.4D4 Board)                                                   | All-in-One Geräte |
|-------------------------------------------------------|------------------------------------------------------------------------------------|--------------------|
| Sollte der Spezifikation "Empfohlen" entsprechen  | Entspricht der Spezifikation "Angemessen"                                     | Von Spezifikation "Minimum" (einfacher Haushalt) bis "Empfohlen" (Unternehmen)                  |
| Austauschbare Komponenten / erweiterbar                | Abgestimmte Komponenten für OPNsense                                                 | Einfach bestellen und los geht's                   |
| Ausreichend Steckplätze und Ports für beliebige Erweiterungen | Sehr niedriger Stromverbrauch                                                       | Abgestimmte Komponenten                   |
| Durchaus einsetzbar in kleineren Unternehmen                    | Im Vergleich zu dem Bauvorschlag oder den All-in-One Geräten sehr preiswert |  In der Regel mit verschiedenen Betriebssystemen bestellbar                  |
| Auch für andere Zwecke (anstatt Router) verwendbar                                                     | Für den normalen Haushalt vollkommen ausreichend                                          |                    |
|       | Sehr leise weil kein Lüfter benötigt wird   |                    |

<br/><br/>

## Nachteile
| Bauvorschlag                                  | Vorschlag Entwicklerboard (APU.4D4 Board)                                                   | All-in-One Geräte |
|-------------------------------------------------------|------------------------------------------------------------------------------------|--------------------|
| Hoher Stromverbrauch                          | RAM und CPU nicht austauschbar                                              | keine austauschbaren Komponenten   |
| im Vergleich zum Entwicklerboard relativ teuer   | nur 1 GHz CPU                            | sehr teuer im Verhältnis zur Leistung und den anderen beiden Optionen     |
| viel mehr Leistung als im normalen Haushalt benötigt wird  |                                                                         |                   |

<br/><br/>

# Sonstiges
In diesem Bereich werden Themen angesprochen, die Einfluss in die Auswahl der Hardware nehmen könnten aber nicht unbedingt hardwarespezifisch sind.
<br/><br/>

## Durchsatz Thematik mit BSD
Aktuell ist der maximale Durchsatz, der mit einem BSD Betriebssystem erreicht werden kann, signifikant geringer als mit einem Linux Betriebssystem. Im Speziellen wurde mir dabei von einer Messung berichtet, bei der mit einem Linux System und dem APU.4D4 maximal 140 mbps Durchsatz erreicht werden konnten. Bei der gleichen Hardware hat das BSD System (pfSense) maximal 100 mbps erreicht. Der Grund dafür soll im Kernel liegen. Mit der nächsten Kernelversion soll das Problem bzw. dieser Unterschied abgeschafft werden, wodurch BSD dann die selbe Leistung wie Linux erreichen soll.

Wichtig hierbei ist zu erwähnen, dass es beim Surfen oder normalen Office Arbeiten keinen Unterschied machen wird, ob 100 mbps oder 140 mbps möglich sind. Ohne Gbit-Leitung werden diese Werte genrell nicht erreicht.
<br/><br/>

## Unterstützung von Netzwerkkarten
Auch bei der Unterstützung der Netzwerkkarten hat Linux die Nase klar vorn. Während Linux sehr viele verschiedene Netzwerkkarten und damit auch die Standards 802.11ac/a/b/g/n (Wi-Fi 5) unterstützt, sind jedenfalls in Bezug auf den APU Board Vorschlag "nur" die Standards 802.11a/b/g/n (Wi-Fi 4) verfügbar. Jedoch soll daran auch schon seit längerem gearbeitet werden. Scheinbar soll es für das Thema W-Lan bei BSD nur einen Maintainer gegeben, der daran in seiner Freizeit arbeitet. Sodass eine Lösung hierfür noch etwas auf sich warten lassen könnte.

Jedoch auch hier macht es im "normalen" Haushalt keinen Unterschied, da auch hier die meisten Haushalte nicht ans Limit kommen werden. Außerdem könnte im Falle des APU-Boards auch ganz auf die Netzwerkkarte verzichtet und ein externer AP (bspw. mit openWRT) angeschlossen werden, wodurch man sich bei aktuellen Geräten keine Sorge um die Leistung und WiFi 5 machen müsste. Eine andere Möglichkeit wäre das APU Board mit bis zu 2 Netzwerkkarten und insgesamt 6 Antennen auszustatten, wodurch theoretisch eine Leistung von 6 x 100 mbps (max. WiFi 4) erreicht werden könnte.
<br/><br/>

# Links
Sammlung und Übersicht verschiedener Hardware Hersteller
- https://friendly-router.org/database/#list-of-manufacturers

OPNsense Dokumentation
- https://docs.opnsense.org/manual/hardware.html
- https://teklager.se/en/knowledge-base/apu2-1-gigabit-throughput-pfsense/

Mikrotik mit openWRT
- https://openwrt.org/toh/mikrotik/common
<br/><br/><br/><br/>













# ####English####
Here follows the collection of all information about hardware for linux router.
<br/><br/>

# Hardware expectations
- AES-NI (intel) support for proper VPN compatibility.
- Multiple access points should be possible
- at least 4 LAN ports
- 2,4 GHz WIFI
- 5 GHz WIFI
- As energy efficient as possible
- In the best case open hardware
<br/><br/>

# Hardware requierments
The following requirements are from the OPNsense documentation.
<br/><br/>

## General requirements
For substantially narrowed OPNsense® functionality there is the basic specification. For full functionality there are minimum, reasonable and recommended specifications.

### Specs
| Hardware       | Minimum                                                                 | Reasonable                                                              | Recommended                   |
|----------------|-------------------------------------------------------------------------|-------------------------------------------------------------------------|-------------------------------|
| Processor      | 1 GHz dual core cpu                                                     | 1 GHz dual core cpu                                                     | 1.5 GHz multi core cpu        |
| RAM            | 2 GB                                                                    | 4 GB                                                                    | 8 GB                          |
| Install method | Serial console or video (vga)                                           | Serial console or video (vga)                                           | Serial console or video (vga) |
| Install target | SD or CF card with a minimum of 4 GB, use nano images for installation. | 40 GB SSD, a minimum of 2 GB memory is needed for the installer to run. | 120 GB SSD                    |

<br/><br/>

### What does it mean?
#### Minimum
The minimum specification to run all OPNsense standard features that do not need disk writes, means you can run all standard features, except for the ones that require disk writes, e.g. a caching proxy (cache) or intrusion detection and prevention (alert database).

#### Reasonable
The reasonable specification to run all OPNsense standard features, means every feature is functional, but perhaps not with a lot of users or high loads.

#### Recommended
The recommended specification to run all OPNsense standard features, means every feature is functional and fits most use cases.

### Throughput
The main hardware-factors of the OPNsense setup involved are CPU, RAM, mass storage (disc), the number and quality of network interfaces.

| Throughput (Mbps) | Hardware requirements | Feature set | Users / Networks       |
|-------------------|-----------------------|-------------|------------------------|
| 11-150            | Basic spec.           | narrowed    | adjusted (10-30)       |
| 11-150            | Minimum spec.         | reduced     | adjusted (10-30)       |
| 151-350           | Reasonable spec.      | all         | substantial (30-50)    |
| 350-750+          | Recommended spec.     | all         | substantial+ (50-150+) |


<br/><br/>

### Further information
While most features do not affect hardware dimensioning, a few features have massive impact on it. The candidates are:

**Squid** <br/>
a caching web proxy which can be used for web-content control, respectively. These packages rely strongly on CPU load and disk-cache writes.

**Captive portal** <br/>
settings with hundreds of simultaneously served captive portal users will require more CPU power in all the hardware specifications displayed below.

**State transition tables** <br/>
it is a known fact, that each state table entry requires about 1 kB (kilobytes) of RAM. The average state table, filled with 1000 entries will occupy about ~10 MB (megabytes) of RAM. OPNsense usage settings with hundred of thousands of connections will require memory accordingly.


# Devices
## All-in-One Devices
- Turris Omnia: https://www.turris.com/en/omnia/overview/
- OPNsense: https://shop.opnsense.com/
- Traverse Ten64: https://www.crowdsupply.com/traverse-technologies/ten64
- Protectli: https://protectli.com/vault-6-port/
- LES compact 4L: https://www.thomas-krenn.com/de/produkte/low-energy-systeme/les-compact-4l/les-compact-4-l.html
- NRG Systems: https://www.ipu-system.de/
- AWOW AK34 Mini PC: https://www.yuenx.com/2019/review-awow-ak34-mini-pc/
  <br/><br/>

## Dev Boards
- jetway nf592-q170: https://www.jetwaycomputer.com/NF592.html
- Apu Board: https://www.pcengines.ch/apu2.htm
- COMMELL: http://www.commell.com.tw/
- BananaPi BPI R64:  http://www.banana-pi.com/eacp_view.asp?id=129 
<br/><br/>

### Hardware configuration
#### Example config

| Config        | Example hardware                                               | Costs in Euro |
|---------------|----------------------------------------------------------------|---------------|
| Prozessor     | INTEL Celeron G5900T 3.2GHz LGA1200 2M Cache Tray              | 45            |
| RAM           | 8GB G.Skill Aegis DDR4-2400 DIMM CL17 Single                   | 50            |
| Mainboard     | Gigabyte B460M Gaming HD, µATX, So. 1200                       | 75            |
| CPU Fan       | Xilence I250PWM Topblow Kühler                                 | 10            |
| Power supply  | 300 Watt be quiet! System Power B9 Bulk Non-Modular 80+ Bronze | 60            |
| Case          | Kolink KLM-002 Micro-ATX Mini-Gehäuse - black                  | 35            |
| Network card  | -                                                              | 50-100        |
| Hard disk     | 120GB HP EX900 M.2 2280 PCIe 3.0 x4 32Gb/s 3D-NAND TLC         | 25            |
|               |                                                                |               |
| **Sum**       |                                                                | **350 - 400** |

<br/>

**TDP: estimated 50 Watt**

<br/><br/>

#### Dev board example
| Dev board example         | construction proposal                                       | Costs in Euro |
|---------------------------|-------------------------------------------------------------|---------------|
| Dev board                 | PC Engines APU.4D4 Board (4 GByte, 4xLAN)                   | 155           |
| Case                      | PC Engines in black, blue or red                            | 13            |
| power supply              | PC Engines                                                  | 6             |
| Hard disk                 | Transcend MTS430S M.2 SATA SSD 256GB incl. adapter          | 48            |
|                           |                                                             |               |
| Available options         |                                                             |               |
| Network card              | Compex WLE200NX 802.11a/b/g/n (Wi-Fi 4) Mini-PCIe-Karte     | 23            |
| Antenna cabel             | 2x Pigtail from U.FL to RP-SMA 15cm                         | 8             |
| Antenna                   | 2x 5dBi Wireless LAN Antenna                                | 8             |
| Preinstallation OPNsense | OPNsense                                                   | 10            |
|                           |                                                             |               |
| **Sum**                   |                                                             | **222** respectively **271** |

<br/>

**TDP: 6-12 Watt**

<br/><br/>

# Pros & Cons
The respective advantages and disadvantages between the all-in-one devices, construction proposal and development boards are listed here.

## Pros
| Construction porposal                                  | Development board proposal (APU.4D4 Board)                                  | All-in-One Devices |
|-------------------------------------------------------|------------------------------------------------------------------------------------|--------------------|
| Should comply with the specification "Recommended"  | Complies with the specification "Reasonable"                                     | From specification "Minimum" (simple household) to "Recommended" (enterprise)                   |
| Exchangeable components / expandable                | Matched components for OPNsense and more    | Just order and go  |
| Sufficient slots and ports for arbitrary expansions | Very low power consumption               | Matched components                   |
| Can be used in smaller companies                    | Very inexpensive compared to the construction proposal or the all-in-one devices | Usually orderable with different operating systems                   |
| Can also be used for other purposes (instead of router)   | Perfectly adequate for normal household                                   |                    |
|       | Very quiet because no fan is needed   |                    |

<br/><br/>

## Cons
| Construction porposal                                 | Development board proposal (APU.4D4 Board)                                  | All-in-One Devices |
|-------------------------------------------------------|------------------------------------------------------------------------------------|--------------------|
| High power consumption                                | RAM and CPU not replaceable                                                    | no replaceable parts    |
| relatively expensive compared to the developer board  | only 1 GHz of CPU      | very expensive in relation to the performance and the other two options     |
| overpowered for most housholds                        |                                                                                   |                   |

<br/><br/>

# Other
## Throughput issue with BSD
Currently, the maximum throughput that can be achieved with a BSD operating system is significantly lower than with a Linux operating system. Specifically, I was told of a measurement where a maximum of 140 mbps throughput could be achieved with a Linux system and the APU.4D4. With the same hardware, the BSD system (pfSense) achieved a maximum of 100 mbps. The reason for this is said to be in the kernel. With the next kernel version, the problem or this difference should be solved, whereby BSD should then achieve the same performance as Linux.

It is important to mention that for surfing or normal office work it will not make any difference if 100 mbps or 140 mbps are possible. Without a Gigabit fiber optics cable, these values are generally not achieved.
<br/><br/>

## Network card support
Linux is also clearly ahead in terms of network card support. While Linux supports many different network cards and thus also the 802.11ac/a/b/g/n (Wi-Fi 5) standards, "only" the 802.11a/b/g/n (Wi-Fi 4) standards are available in terms of the APU board proposal. However, this has been in the works for quite some time. Apparently there should be only one maintainer for the topic W-Lan with BSD, who works on it in its spare time. So that a solution for this could still let wait somewhat for itself.

However, it doesn't make any difference in the "normal" household, since most households won't reach the limit here either. Furthermore, in the case of the APU board, the network card could be completely omitted and an external AP (e.g. with openWRT) could be connected, which means that you don't have to worry about the performance and WiFi 5 with current devices. Another possibility would be to equip the APU board with up to 2 network cards and a total of 6 antennas, which could theoretically achieve a performance of 6 x 100 mbps (max. WiFi 4).
<br/><br/>

# Links
List of manufacturers
- https://friendly-router.org/database/#list-of-manufacturers
  
OPNsense documentation
- https://docs.opnsense.org/manual/hardware.html
- https://teklager.se/en/knowledge-base/apu2-1-gigabit-throughput-pfsense/

Mikrotik with openWRT
- https://openwrt.org/toh/mikrotik/common