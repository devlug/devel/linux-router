####English version below####
<br/><br/>

# Installation der OPNsense auf dem APU 4D4
## Download
Vor der Installation von OPNsense muss sich Gedanken über die Art der Installation gemacht werden, weil sich die Installationsdateien unterscheiden, je nachdem auf welche Art man OPNsense installieren möchte. Ich habe OPNsense über die serielle RS232 Schnittstelle installiert. Die richtige Datei für die jeweilige Installationsart wird durch den fett markierten Teil im Dateinamen hervorgehoben.

Zuerst werden die zur Installation und Verfikation benötigten Dateien aus dem [Downloadbereich von OPNsense](https://opnsense.org/download/) heruntergeladen. <br>
Zur Installation und Verifizierung werden die folgenden 4 Dateien benötigt:
- Die bzip komprimmierte ISO-Datei (OPNsense-21.1-OpenSSL-**serial**-amd64.img.bz2   .iso.bz2)

- Die SHA-256 Checksummen Datei (OPNsense-21.1-OpenSSL-checksums-amd64.sha256)

- Die Signatur Datei (OPNsense-21.1-OpenSSL-**serial**-amd64.img.bz2.sig)

- Der openssl public key (OPNsense-21.1.pub)

> Hinweis: Die aktuelle Version ist OPNsense 21.1, die Version kann sich im Laufe der Zeit selbstverständlich ändern, sodass der Dateiname ein anderer wird.

Im Downloadverzeichnis werden anschließend die folgenden zwei Befehle ausgeführt:  
`openssl base64 -d -in OPNsense-21.1-OpenSSL-serial-amd64.img.bz2.sig -out /tmp/image.sig`  
und   
`openssl dgst -sha256 -verify OPNsense-21.1.pub -signature /tmp/image.sig OPNsense-21.1-OpenSSL-serial-amd64.img.bz2`   

Das erfolgreiche Ergebnis sollte wie folgt aussehen:

![verify_download](./images/verify_download.png)

Im nächsten Schritt wird ein bootfähiger USB-Stick erstellt.

## Bootfähiger USB-Stick
In diesem Kapitel wird ein bootfähiger USB-Stick mit Linux Boardmitteln erstellt. Dieser benötigt mindestens eine Speichergröße von 2GB.  
Bevor das Image auf den USB-Stick geschrieben werden kann, muss das Image erstmal entpackt werden. Dafür wird der folgende Befehl im Downloadverzeichnis ausgeführt:   
`bunzip2 OPNsense-21.1-OpenSSL-serial-amd64.img.bz2`  

Zusätzlich muss der USB-Stick für das Schreiben des Images ausgehangen werden, dies geschieht mit dem folgenden Befehl:  
`sudo umount /dev/sdX[1]`  

> Hinweis: Das X ist ein Platzhalter für den jeweilig im System registrierten und benannten Datenträger (USB-Stick). Die 1 steht für die gemountete Partition, auch die Zahl kann unter bestimmten Voraussetzungen variieren.  

Anschließend wird im Downloadverzeichnis oder dem Verzeichnis, indem das Image gespeichert ist, der folgende Befehl ausgeführt:  
`sudo dd status=progress if=OPNsense-21.1-OpenSSL-serial-amd64.img of=/dev/sdb bs=1M && sync`   
Damit wird das Image auf den USB-Stick gebrannt. Der erfolgreiche Durchlauf sollte wie folgt aussehen:  

![succeded_burn](./images/burn_ready.png)  

## Installation
Jetzt kann es endlich an die eigentliche Installation von OPNsense gehen. Dafür wird der USB-Stick in den Router gesteckt. Anschließend wird das serielle Nullmodem-zu-USB-Kabel mit Rechner und Router verbunden. Die Erst-Konfiguration von WAN und LAN wird automatisiert eingerichtet, wenn LAN und WAN ebenfalls angeschlossen werden. Dafür wird das LAN Kabel in den Computer und in den ganz linken LAN Port (igb0) eingesteckt. Das WAN Kabel kommt in den zweiten Port von links (igb1).  Der Router wird von der Seite betrachtet, sodass sich der serielle RS-232 Anschluss auf der linken Seite befindet.   
Mit dem Programm Minicom kann nun eine serielle Verbindung aufgebaut werden. Hierfür werden die folgenden Parameter konfiguriert:

![config_minicom](./images/minicom.png)

Ist Minicom richtig konfiguriert, sollte die Bildschirmausgabe wie folgt aussehen:

![started_minicom](./images/connected_minicom.png)

Anschließend wird der Router an den Strom angeschlossen. Beim Start des Routers jetzt unbedingt auf F10 drücken, um in das Bootmenü zu gelangen. Ist der USB-Stick richtig erstellt, wird der USB-Stick unter 1. angezeigt. Hat dies geklappt, wird der folgende Bildschirm ausgegeben:

![boot_usb](./images/booting_usb.png)

Mit einem Klick auf die 1 bootet die OPNsense Installation....   
Login: installer  
Passwort: opnsense   
Der Installer begrüßt uns nun mit dem folgenden Bildschirm:  

![start_installation](./images/startScreen.png)
 
 Mit "Enter" wird bestätigt, dass es mit der Installation losgehen soll. In der nächsten Ausgabe wird bestätigt, dass eine geführte Installation (Guided installation) ausgeführt werden soll:

![guided_installation](./images/guidedInstallation.png)

Im nächsten Schritt muss ein Medium für die Installation von OPNsense ausgewählt werden. Hierbei aufpassen, dass nicht der USB-Stick ausgewählt wird.   

Anschließend wird GTP/UEFI mode ausgewählt, das passt in der Regel für die APU-Boards.  

Jetzt warten wir das Partitionieren und Installieren ab. Zwischendurch wird um ein starkes ROOT Passwort gebeten.   
> WICHTIG: Setzt ein ordentliches und starkes Passwort!

Ist die Installation abgeschlossen, muss der Router neugestartet werden. Dafür "Reboot" mit Enter bestätigen. Sobald der Router neustartet, gelangt man mit "F10" wieder in das Bootmenü. Dies sollte diesmal wie folgt aussehen:

![reboot](./images/boot_menue.png)

Unter 2. ist nun die Festplatte angezeigt, auf der das OPNsense installiert wurde.   
Herzlichen Glückwunsch, die Installation ist erfolgreich durchgelaufen! Einfach auf die 2 drücken und schon fährt eure OPNsense hoch. Der USB-Stick kann nun herausgezogen werden.   

Im nächsten Schritt kann es dann mit der Konfiguration losgehen!

<br>

# ####English####

## Installation of the OPNsense on the APU 4D4
## Download
Before installing OPNsense you have to think about the way of installation, because the installation files differ depending on the way you want to install OPNsense. I have installed OPNsense via the serial RS232 interface. The correct file for the particular installation type is highlighted by the bold part in the file name.

First, the files needed for installation and verification are downloaded from the [download area of OPNsense](https://opnsense.org/download/). <br>
The following 4 files are needed for installation and verification:
- The bzip compressed ISO file (OPNsense-21.1-OpenSSL-**serial**-amd64.img.bz2 .iso.bz2).

- The SHA-256 checksum file (OPNsense-21.1-OpenSSL-checksums-amd64.sha256)

- The signature file (OPNsense-21.1-OpenSSL-**serial**-amd64.img.bz2.sig)

- The openssl public key (OPNsense-21.1.pub)

> Note: The current version is OPNsense 21.1, the version may of course change over time, so the filename will be different.

The following two commands are then executed in the download directory:  
`openssl base64 -d -in OPNsense-21.1-OpenSSL-serial-amd64.img.bz2.sig -out /tmp/image.sig`  
and   
`openssl dgst -sha256 -verify OPNsense-21.1.pub -signature /tmp/image.sig OPNsense-21.1-OpenSSL-serial-amd64.img.bz2`   

The successful result should look like this:

![verify_download](./images/verify_download.png)

The next step is to create a bootable USB stick.

## Bootable USB-Stick
In this chapter a bootable USB stick is created with Linux board means. This needs at least a memory size of 2GB on USB.  
Before the image can be written to the USB stick, the image must first be unpacked. For this the following command is executed in the download directory:   
`bunzip2 OPNsense-21.1-OpenSSL-serial-amd64.img.bz2`.  

Additionally, the USB stick must be unmounted for writing the image, this is done with the following command:  
`sudo umount /dev/sdX[1]`  

> Note: The X is a placeholder for the respective volume (USB stick) registered and named in the system. The 1 stands for the mounted partition, also the number can vary under certain circumstances.  

Afterwards the following command is executed in the download directory or the directory where the image is stored:  
`sudo dd status=progress if=OPNsense-21.1-OpenSSL-serial-amd64.img of=/dev/sdb bs=1M && sync`.   
This will burn the image to the USB stick. The successful burn should look like this:  

![succeded_burn](./images/burn_ready.png)

## Installation
Now you can finally start the actual installation of OPNsense. For this, the USB stick is plugged into the router. Then the serial null modem to USB cable is connected to computer and router. The initial configuration of WAN and LAN is set up automatically when LAN and WAN are also connected. For this, the LAN cable is plugged into the computer and into the leftmost LAN port (igb0). The WAN cable goes into the second port from the left (igb1).  The router is viewed from the side so that the RS-232 serial port is on the left side.   
Now a serial connection can be established with the Minicom program. For this the following parameters are configured:

![config_minicom](./images/minicom.png)

If Minicom is configured correctly, the screen output should look like this:

![started_minicom](./images/connected_minicom.png)

Next, connect the router to power. When starting the router now be sure to press F10 to get into the boot menu. If the USB stick is created correctly, the USB stick will be displayed under 1. If this has worked, the following screen is displayed:

![boot_usb](./images/booting_usb.png)

With a click on 1 the OPNsense installation boots.....   
Login: installer  
Password: opnsense   
The installer now greets us with the following screen:  

![start_installation](./images/startScreen.png)
 
 With "Enter" it is confirmed that the installation should start. The next output confirms that a guided installation should be executed:

![guided_installation](./images/guidedInstallation.png)

The next step is to select a medium for the installation of OPNsense. Be careful not to select the USB stick.   

Afterwards GTP/UEFI mode is selected, this usually fits for the APU boards.  

Now we wait for the partitioning and installation. In between we are asked for a strong ROOT password.   
> IMPORTANT: Set a proper and strong password!

If the installation is finished, the router must be rebooted. Confirm "Reboot" with Enter. As soon as the router reboots, you get back to the boot menu by pressing "F10". This time it should look like this:

![reboot](./images/boot_menue.png)

Under 2. you can now see the harddisk where the OPNsense has been installed.   
Congratulations, the installation has been completed successfully! Just press 2 and your OPNsense will boot. The USB stick can now be pulled out.   

In the next step you can start with the configuration!