# Some examples with ansible
<br/><br/>

## create admin user on opnsense machine
Add via GUI: System → Access → User (don't forget to add authorized_keys) or via shell (and copy ssh-key into .ssh/authorized_keys):

    # useradd

## Add wheel und admin group
Edit /etc/group and add:

    # wheel:*:0:root,admin
    # admins:*:1999:root,admin

## enable sudo for admin

    # visudo
Uncomment this line:

    # Uncomment to allow members of group wheel to execute any command
    %wheel ALL=ALL(ALL) ALL

## Customize path for python in ansibel-hosts file:

    ansible_python_interpreter=/usr/local/bin/python3


## Examples

### Role: opnsense_backup

Save config.xml to ansible host

### Role: opnsense_pkg

Package update (no upgrade, no reboot requred)
Install of wanted packages
Deployment of Bash Aliases (.cshrc)

### Role: opnsense_deploy_checkmk

Download and installation of check_mk agents

Note: Is is requred to add firewall rule to make agent reachable (Port 6556/TCP) 
